#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

 int convertir(char s){
     int i, valor;
     int r=0;
     switch (s)
     {
     case 'I':valor=1;break;
     case 'V':valor=5;break;
     case 'X':valor=10;break;
     case 'L':valor=50;break;
     case 'C':valor=100;break;
     case 'D':valor=500;break;
     case 'M':valor=1000;break;
     default: break;
     }
    r=valor;
    return r;
 }
    
    int obtenerTipo(int uno,int dos){
        int tipo;
    if (uno>=dos)
       {
           tipo=1;
           return tipo;
       }else if(uno<dos)
       {
           tipo=2;
           return tipo;
       }
    }

int obtenerResultado(int primerPar,int segundoPar, int tipo){
        int resultado;
    switch (tipo)
    {
    case 1: 
        resultado = primerPar + segundoPar;
        return resultado;
    break;
    case 2: 
        resultado = segundoPar - primerPar;
        return resultado;
    break;
    default:
        break;
    }
    }    

int main()
{
    int d1,d2,primerPar,segundoPar, resultado, tipo;
    int r1[1];
    int r2[1];
    char cadena[4];
    int contador = 0;

    printf("Ingresa ambos pares sin dejar espacio: ");
    scanf("%s", cadena);

     while ( cadena[contador] != 0) {
        contador++;    
    } 
     if (contador == 4 && isalpha(cadena[1]) && isalpha(cadena[3]) && isdigit(cadena[0]) && isdigit(cadena[2])){
        
        r1[0]=convertir(cadena[1]);
        r2[0]=convertir(cadena[3]);
        d1= cadena[0] - '0';
        d2= cadena[2] - '0';
        primerPar = r1[0] * d1;
        segundoPar = r2[0] * d2;
        
        tipo = obtenerTipo(r1[0],r2[0]);
        resultado = obtenerResultado(primerPar,segundoPar,tipo);

        printf("El valor de la cadena es %i",resultado);
        
    }
    else
    {
        printf("No es valido, ingresa dos pares AR");
    }
}
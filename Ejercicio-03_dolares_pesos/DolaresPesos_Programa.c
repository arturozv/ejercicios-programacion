#include <stdio.h>

int main()
{
    double COSTO;
	double dolares, pesos;
	
	COSTO = 21.03;
	dolares = 0;
	pesos = 0;
    
    printf("Ingresa los dolares a convertir: ");
    scanf("%lf", &dolares);
    	pesos = dolares * COSTO;
    printf("%.lf dolares a pesos mexicanos son: %.1lf", dolares, pesos);
            
    return 0;
}
